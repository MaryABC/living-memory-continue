package com.example.administrator.livingmemory;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MainActivity extends AppCompatActivity {

    SurfaceView cameraView;
    CameraSource cameraSource;
    ReentrantLock lock1;
    boolean isVideoRunning=false, canStartNewVideo=true;
    Context context;

    //each row contain 3 elements: the name of the buried, the text on the grave, video link
    String[][] gravetextVideolink = {
            {"Tmima Hillel","tamima hillel the wife of shlomo hillel speaker of the 11th knesset of the state of israel daughter of aranka and ephraim  rosner born 5.6.1928 died 12.12.2011","xxx"},
            {"Yitzhak Shamir","yitzhak shamir yezernitzky seventh prime minister of the state of israel speaker of the knesset minister of foreign affairs minister of defence minister of labor and social welfare senior officer in the mossad a leader in the lehi a commander in the etzel son of pnina and shlomo sarah shulamit shamir the wife of yitzhak shamir the seventh prime minister of the state of israel daughter of hana and eliahu levy","https://www.youtube.com/watch?v=hSjWip8CnDg"},
            {"Shimon Peres","shimon peres son of sarah Meltzer and yitzhak getzel perski vishnyeva 2.8.1923 tel aviv 28.9.2016 one of the founding fathers of israel which he served his entire life. ninth president of the state of israel. prime minister, minister of defense, minister of foreign affairs and minister of finance. among those who laid the foundations for israel's strategic deterrence and defense capabilities, one of the founders of the reactor in dimona and of israels defense industry. led israel to become an innovation and technological superpower. Worked tirelessly to advance peace between israel and her neighbors and to strengthen tolerance and coexistence within israeli society. graduate of ben shemen and a founder of alumot. statesman lover of literature poetry and art nobel peace prize laureate. a man of vision and action","https://www.youtube.com/watch?v=8K9TVcpvj8o"},
            {"Shneur Zalman Shazar","shneur zalman shazar rubashov son of shara and yehuda leib born 1889 died 1974 third president of the state of israel 1963 1973 chairman of the zionist executive jewish agency 1956 1960 first minister of education and culture 1949 1951 member of the first knesset rahel katznelson shazar his wife daughter of zelda lane and nissan born 1885 died 1975","https://www.youtube.com/watch?v=7y33l23C6F8"},
            {"Berman Isaac","berman isaac son of rabbi yaacov and miryam berman born 3.6.1913 died 4.8.2013 speaker of the ninth knesset energy and infrastructure minister during the tenth knesset commander of british intelligence network that operated on behalf of the etzel in the balkans during the second world war","https://www.youtube.com/watch?v=U4WpqAjaagM"},
            {"Chaim Herzog","chaim herzog the sixth president 17 september 1918 17 april 1997 son of chief rabbi isaac halevi herzog and rabbznit sarah a founder and director of the idf military intelligence 1948 1950 1959 1962 major general in the idf first military governor of jerusalem judea and samaria 1967 ambassador to the united nations 1975 1978 member of the knesset 1981 1983 president of the state of israel may 1983 1993 soldier statesman man of law and letters","https://www.youtube.com/watch?v=whpPTzoIwdE"},
            {"Yitzhak Navon","yitzhak navon fifth president of the state of israel minister of education and culture deputy prime minister member of knesset chairman of the foreign affairs and defense committee secretary and confidant of david ben-gurion a native of jerusalem who defended the city as a member of the hagana authour and playwright teacher and mentor a lover of if-mankind and a seeker of peace endeavoured to bridge the gaps in israeli society son of miriam and yossef april 9 1921 november 6 2015 ofira navon the wife of the fifth president of the state of israel daughter of batya and eliezer erez born 1936 died 1993","https://www.youtube.com/watch?v=r-UAPWwF47g"},
            {"Yitzhak Rabin","yitzhak rabin son of roza and nehemiah born 1992 assassinated 1995 chief of general staff during the 1967 six day war ambassador of israel to u.s.a 1968 1973 prime minister 1974 1977 minister of defence 1984 1990 prime minister and minister of defence 1992 1995 lea rabin daughter of gusta and chaim fima born 1928 died 2000 carrier of the torch","https://www.youtube.com/watch?v=6ykSa1ygMY4"},
            {"Israel Yeshayahu","israel yeshayahu son of shoshana and yeshayahu sharabi born 1911 died 1979 speaker of the knesset 1972 1977 deputy speaker of the knesset 1955 1967 minister of post 1967 1970 member of the israli knesset 1949 1977 rina yeshayahu his wife daughter of naama and yosef badichi born 1913 died 1986","https://www.youtube.com/watch?v=N79UuPC0UIU"},
            {"Golda Meir Meirson","golda meir meirson daughter of bluma and moshe mabovitz born 1898 died 1978 prime minister 1969 1974 minister of foreign affairs 1956 1966 minister of labor 1949 1956 minister of israel in moscow 1948 1949 member of the first to eighth knesset","https://www.youtube.com/watch?v=8SErijDT1Tc"},
            {"Levi Eshkol","levi eshkol third prime minister of the state of israel 1963 1969  minister of defence minister of finance a pioneer man of vision and action man of soil and water builder of israel and its economy a lover of mankind and pursuer of peace son of dvora and yosef shkolnik miriam eshkol wife of prime minister levi eshkol a scholar and a woman of culture daughter of rebecca and avraham zelikowitz","https://www.youtube.com/watch?v=OQlixvGIfXM"},
            {"Joseph Sprinzak","joseph sprinzak son of bella and dov ber born 1885 died 1959 chairman of provisional council of state speaker of the first to third knesset hana sprinzak his wife daughter of leah and yitzhak wanetik","https://www.youtube.com/watch?v=fnyRI6_418k"},
            {"Eliezer kaplan","eliezer kaplan son of frooma and meir born 1891 died 1952 deputy prime minister first minister of finance member of the first and second knesset dvora kaplan his wife pediatrician daughter of keyla and abraham kaplan","https://www.youtube.com/watch?v=jrfhx_3xkLw"},
            {"Teddy Theodor Kollek","teddy theodor kollek son of grete rachel and alfred abraham mayor of jerusalem active in rescuing from europe on behalf of the jewish agency head of the arms acquisitions program for the hagana in the u.s.a consul in the israeli embassy in washington director general of the office of prime minister david ben gurion tamar anna kollek wife of the mayor of jerusalem daughter of alice and rabbi dr. zacharias schwarz born 17.7.1917 died 25.7.2013","https://www.youtube.com/watch?v=SAJ-5gouZkM"},
            {"Menahem Savidor","menahem savidor son of anna and dov chodorowski born 1917 died 1988 speaker of the tenth knesset 1981 1984 member of the ninth and tenth knesset raia savidor his wife daughter of liza eta and dr. yitzhak karukes born 1929 died 1988","https://www.youtube.com/watch?v=GngkvZtLeu0"},
            };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if(requestCode==111)
            isVideoRunning=false;
        else
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode)
        {
            case 1001:
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    if(ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                        return;
                    try {
                        cameraSource.start(cameraView.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context=this;

        GPSTracker gps = new GPSTracker (this, MainActivity.this);
        Toast.makeText(this,gps.getLatitude()+" "+gps.getLongitude(),Toast.LENGTH_LONG).show();
        Toast.makeText(this,"Welcome to Mount Herzl!",Toast.LENGTH_LONG).show();
        /*
        Intent t = new Intent(this, Main2Activity.class);
        t.putExtra("value",gps.getLatitude()+" "+gps.getLongitude());
        startActivity(t);
  *//*
        if(gps.getLatitude()>31.7753 || gps.getLatitude()<31.7752 ||gps.getLongitude()>35.1793 || gps.getLongitude()<35.1792)
        {
            Toast.makeText(this,"you're not in the right position",Toast.LENGTH_LONG).show();
            return;
        }*/

        cameraView = (SurfaceView) findViewById(R.id.surface_view);
        lock1 = new ReentrantLock();

        TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();
        if (!textRecognizer.isOperational()) {
            Log.w("MainActivity", "Detector dependencies are not available");
        } else {
            cameraSource = new CameraSource.Builder(getApplicationContext(), textRecognizer)
                    .setFacing(CameraSource.CAMERA_FACING_BACK)
                    .setRequestedPreviewSize(1280, 1024)
                    .setRequestedFps(2.0f)
                    .setAutoFocusEnabled(true)
                    .build();
            cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {

                    try {
                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.CAMERA}, 1001);
                            return;
                        }
                        cameraSource.start(cameraView.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

                }

                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {
                    cameraSource.stop();
                }
            });
            textRecognizer.setProcessor(new Detector.Processor<TextBlock>() {
                @Override
                public void release() {

                }

                @Override
                public void receiveDetections(Detector.Detections<TextBlock> detections) {
                    final SparseArray<TextBlock> items = detections.getDetectedItems();
                    if(items.size()!=0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        for(int i=0; i<items.size(); i++)
                        {
                            TextBlock item = items.valueAt(i);
                            stringBuilder.append(item.getValue());
                            stringBuilder.append("\n");
                        }
                        String detection=stringBuilder.toString().toLowerCase();

                        int matchIndex=-1;
                        for(int i=0; i<gravetextVideolink.length && matchIndex==-1; i++)
                        {
                            String[] graveWords=gravetextVideolink[i][1].split(" ");
                            int wordCount=0;
                            for(int j=0; j<graveWords.length; j++)
                            {
                                if(detection.contains(graveWords[j])) wordCount++;
                            }
                            if(wordCount>graveWords.length*0.6) //we are assuming that ocr is more than 60% accurate
                                matchIndex=i;
                        }
                        if(matchIndex!=-1) {
                            if (lock1.tryLock()) {
                                if (!isVideoRunning && canStartNewVideo) {
                                    isVideoRunning = true;
                                    canStartNewVideo = false;
                                    //Toast.makeText(getApplicationContext(),gravetextVideolink[matchIndex][0],Toast.LENGTH_LONG).show();
                                    next(gravetextVideolink[matchIndex][0],gravetextVideolink[matchIndex][2]);
                                    //startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse(gravetextVideolink[matchIndex][2])),111);
                                } else {

                                    if(!isVideoRunning)
                                        canStartNewVideo=true;
                                }
                                lock1.unlock();
                            }
                        }
                    }
                }
            });
        }
    }

    private void next(String name, String videoUrl)
    {
        if (lock1.tryLock()) {
            try {
                Intent t = new Intent(this, Main2Activity.class);
                t.putExtra("name",name);
                t.putExtra("url",videoUrl);
                startActivityForResult(t,111);
            } finally {
                lock1.unlock();
            }
        }
    }


    public void search_click(View view) {
      /*  ((Button)view).setText("Searching...");

        try {
            Thread.sleep(5000); //lets search for 20 seconds
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ((Button)view).setText("Search2");*/

    }
}
